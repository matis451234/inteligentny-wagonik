import csv
import numpy as np
import math
import random
import time

class Trolley:
    def __init__(self, name, weight, traction, possible_speeds):
        self.name = name
        self.weight = float(weight)                    #in Newtons
        self.traction = float(traction)
        self.possible_speeds = possible_speeds  #in Meters per second list of float
   
    def max_centrifugal_force(self):
        return float(self.weight*self.traction)


class Turn:
    def __init__(self, id, radius, length, next_turns_id):
        self.id = id
        self.radius = float(radius)                     #in Meters
        #self.speed_of_trolley = float(speed_of_trolley)
        self.length = float(length)
        self.next_turns_id = next_turns_id
    
    def centrifugal_force(self, trolley_weight, speed_of_trolley):
        return float((trolley_weight*pow(speed_of_trolley, 2))/self.radius)

class Q_table:
    def __init__(self, dict_turns):
        keys = list(dict_turns.keys())
        self.q_values = np.zeros((2*len(keys)-1, 10))
        i=1
        keys.pop(-1)
        for key in keys:
            turn = dict_turns[key]
            next_turns = turn[2]
            number_of_next_turns = len(next_turns)
            for j in range(number_of_next_turns, 10):
                self.q_values[i][j] = -1000
            i += 2
                
    def change_value(self, state, action, new_value):
        self.q_values[state][action] = new_value
     
    def get_value(self, state, action):
        return self.q_values[state][action]  
    
    def get_state_actions(self, state):
        return self.q_values[state]

class Best_way:
    def __init__(self, trolley):
        self.trolley = trolley
        
    def find_best_ride(self, file_name):
        list_of_max_speeds = []
        list_of_min_times = []
        total_min_time = 0
        dict_turns = read_turns(file_name)
        list_of_turns_id = ['1']
        line = dict_turns[list_of_turns_id[0]]
        turn = Turn(list_of_turns_id[0], line[1], line[2], line[3])
        max_v = math.sqrt(self.trolley.weight*turn.radius)
        min_t = turn.length/max_v
        list_of_max_speeds.append(max_v)
        list_of_min_times.append(min_t)
        total_min_time += min_t
        for turn_id in list_of_turns_id:
            line = dict_turns[turn_id]
            possible_next_turns_id = line[3]
            min_t = None
            next_turn_id = None
            if possible_next_turns_id[0] is not '':
                for possible_turn_id in possible_next_turns_id:
                    line = dict_turns[possible_turn_id]
                    possible_turn = Turn(possible_turn_id, line[1], line[2], line[3])
                    max_v = math.sqrt(self.trolley.weight*possible_turn.radius)
                    if not min_t:
                        min_t = possible_turn.length/max_v
                        next_turn_id = possible_turn.id
                    else:
                        if possible_turn.length/max_v < min_t:
                            min_t = possible_turn.length/max_v
                            next_turn_id = possible_turn.id
                    list_of_max_speeds.append(max_v)
                    list_of_min_times.append(min_t)
                    total_min_time += min_t
                list_of_turns_id.append(next_turn_id)    
        return list_of_turns_id, list_of_max_speeds, list_of_min_times, total_min_time    
            
    def find_best_possible_ride(self, file_name):
        list_of_possible_speeds  = self.trolley.possible_speeds
        list_of_max_speeds = []
        list_of_min_times = []
        total_min_time = 0
        dict_turns = read_turns(file_name)
        list_of_turns_id = [0]
        line = dict_turns[list_of_turns_id[0]]
        turn = Turn(list_of_turns_id[0], line[0], line[1], line[2])
        max_v = math.sqrt(self.trolley.traction*turn.radius)
        max_possible_v = 0
        for possible_speed in list_of_possible_speeds:
            if possible_speed > max_possible_v and possible_speed <= max_v:
                max_possible_v = possible_speed
        if max_possible_v == 0:
                    raise ValueError("Ta trasa jest niemożliwa do pokonania przy takich prędkościach wózka")        
        min_t = turn.length/max_possible_v
        list_of_max_speeds.append(max_possible_v)
        list_of_min_times.append(min_t)
        total_min_time += min_t
        for turn_id in list_of_turns_id:
            line = dict_turns[int(turn_id)]
            possible_next_turns_id = line[2]
            min_t = None
            next_turn_id = None
            if possible_next_turns_id[0] is not '':
                for possible_turn_id in possible_next_turns_id:
                    line = dict_turns[int(possible_turn_id)]
                    possible_turn = Turn(possible_turn_id, line[0], line[1], line[2])
                    max_v = math.sqrt(self.trolley.traction*possible_turn.radius)
                    #print(f'max_v {max_v}')
                    #print(f'possible_speeds {list_of_possible_speeds}')
                    max_possible_v = 0
                    for possible_speed in list_of_possible_speeds:
                        if possible_speed > max_possible_v and possible_speed < max_v:
                            max_possible_v = possible_speed
                    #print(f'max_possible_v {max_possible_v}')
                    if max_possible_v == 0:
                        raise ValueError("Ta trasa jest niemożliwa do pokonania przy takich prędkościach wózka")
                    if not min_t:
                        min_t = possible_turn.length/max_possible_v
                        next_turn_id = possible_turn.id
                    else:
                        if possible_turn.length/max_possible_v< min_t:
                            min_t = possible_turn.length/max_possible_v
                            next_turn_id = possible_turn.id
                list_of_max_speeds.append(max_possible_v)
                list_of_min_times.append(min_t)
                total_min_time += min_t
                list_of_turns_id.append(next_turn_id)    
        return list_of_turns_id, list_of_max_speeds, list_of_min_times, total_min_time       
    
class Learning:
    def __init__(self, trolley, dict_turns, q_table, best_way, quality, file_nmame):
        self.trolley = trolley
        self.dict_turns = dict_turns
        self.q_tabel = q_table
        self.best_way = best_way
        self.quality = quality
        self.file_name = file_nmame
    
    def choose_action(self, state, epsilon):
        actions = self.q_tabel.get_state_actions(state)
        if random.randint(0,100) < epsilon*100:
            possible_actions = []
            i = 0
            for action in actions:
                if int(action) != -1000:
                    possible_actions.append(i)
                i += 1
            return random.choice(possible_actions)   
        else:
            i = 0
            max_value = -1000
            for action in actions:
                if action > max_value:
                    action_with_highest_value = i
                i += 1
            return action_with_highest_value      
            
        
    def learn_trolley_fast_way(self,epsilon, alfa, discount):
        #print(self.q_tabel.q_values)
        total_min_time = self.best_way.find_best_possible_ride(self.file_name)[-1]  
        learned_time = math.inf
        expected_time = total_min_time/self.quality
        end_state = len(self.q_tabel.q_values) - 1
        possible_trolley_speeds = self.trolley.possible_speeds
        iteration = 0
        sr_ridetime = 0
        least_learned_time = math.inf
        while learned_time > expected_time:
            state = 0
            ride_time = 0
            iteration += 1
            crash = False
            list_of_speeds = []
            list_of_turns_id = []
            list_of_turns_next_action = []
            while state != end_state and crash == False:
                actions = self.q_tabel.get_state_actions(state)
                unknown_reward = 0
                for action in actions:
                    if action == 0:
                         unknown_reward += 1
                epsilon_speed = unknown_reward/len(actions)         
                action = self.choose_action(state, epsilon_speed)
                turn = make_turns(self.dict_turns, (state/2))
                speed_of_trolley = possible_trolley_speeds[action]
                list_of_speeds.append(speed_of_trolley)
                list_of_turns_id.append(turn.id)
                
                ride_time += turn.length/speed_of_trolley
                reward = trolley_in_turn(self.trolley, turn, speed_of_trolley)
                self.q_tabel.change_value(state, action, reward)
                if reward == -1000:
                    crash = True
                
                action = self.choose_action(state + 1, epsilon)
                list_of_turns_next_action.append((turn.id, action))
                next_turn_id = turn.next_turns_id[int(action)]
                state = int(next_turn_id) * 2
            
            actions = self.q_tabel.get_state_actions(state)
            unknown_reward = 0
            for action in actions:
                if action == 0:
                    unknown_reward += 1
            epsilon_speed = unknown_reward/len(actions)    
            action = self.choose_action(int(state), epsilon_speed)
            turn = make_turns(self.dict_turns, (state/2))
            speed_of_trolley = possible_trolley_speeds[action]
            list_of_speeds.append(speed_of_trolley)
            list_of_turns_id.append(turn.id)
            ride_time += turn.length/speed_of_trolley
            reward = trolley_in_turn(self.trolley, turn, speed_of_trolley)
            self.q_tabel.change_value(state, action, reward)
            sr_ridetime += ride_time
            
                   
            if reward == -1000:
                    crash = True
            
            if crash == False:
                learned_time = ride_time
                if least_learned_time > learned_time:
                    least_learned_time = learned_time
                
                  
                final_list_of_speeds = list_of_speeds
                final_list_of_turns_id = list_of_turns_id
                j=1
                epsilon = 1 - min(1, total_min_time / ride_time)
                for turn_id_action in list_of_turns_next_action:
                    reward = pow((expected_time/ride_time)*100, 3)
                    old_q_value = self.q_tabel.get_value(2*int(turn_id_action[0]) + 1, int(turn_id_action[1])) 
                    if j < len(list_of_turns_next_action):
                        t_i_a = list_of_turns_next_action[j]
                        old_next_q_max_value = self.q_tabel.get_value(2*int(t_i_a[0]) + 1, int(t_i_a[1])) 
                    else:
                        old_next_q_max_value = 0
                    j+=1    
                    new_q_value = old_q_value + alfa * (reward + discount * old_next_q_max_value - old_q_value )
                    #new_q_value = old_q_value + reward
                    self.q_tabel.change_value(2*int(turn_id_action[0]) + 1, int(turn_id_action[1]), new_q_value)
            if iteration % 100 == 0:
                    print(f'iteracja: {iteration}')
                    print(f'Najmniejszy znaleziony czas: {least_learned_time} Oczekiwany czas: {expected_time}')        
        return list_of_turns_id, list_of_speeds, learned_time, iteration   
            
    
def trolley_in_turn(trolley, turn, speed_of_troley):
    max_centrifugal_force = trolley.max_centrifugal_force()
    centrifugal_force = turn.centrifugal_force(trolley.weight, speed_of_troley)
    float(max_centrifugal_force)
    float(centrifugal_force)
    ratio = centrifugal_force/max_centrifugal_force 
 
    if ratio > 1:
        return -1000
    elif ratio == 1:
        return 10
    elif ratio > 0 and ratio <1:
        return (int(ratio*10) + 1)
    else:
        return -1000

def read_turns(file_name):
    l_id = []
    l_radius = []
    l_length = []
    l_next_turns_id = []
    i=0
    dict_turns = {};
    with open(file_name) as turns:
        reader = csv.reader(turns, delimiter=';')
        next(turns)
        for line in reader:
            l_id.append(int(line[0]))
            l_radius.append(float(line[1]))
            l_length.append(float(line[2]))
            l_next_turns_id.append(line[3].split(' '))
            i += 1
            #next_turns_str = line[3].split(' ')
            #next_turns = [int(x) for x in next_turns_str]
            dict_turn = {int(line[0]) : (float(line[1]), float(line[2]), line[3].split(' '))}
            dict_turns.update(dict_turn)
    # return l_id, l_radius, l_speed_of_trolley,  l_length, l_next_turns_id, dict_turns
    return dict_turns

def make_turns(dict_turns, turn_id):
    line = dict_turns[turn_id]
    turn = Turn(turn_id, line[0], line[1], line[2])
    return turn
    


         

def main():
    file_name = 'turn_rand_super_hard_t.csv'
    possible_speeds = [1, 2, 5, 7, 10, 13, 15, 18, 20, 25]
    wagonik = Trolley("1",30,3, possible_speeds)
    #next_turns_1 = [2,3,4]
    #zakret = Turn(1, 10, 20,next_turns_1)
    #print(trolley_in_turn(wagonik, zakret, 10))
    b = Best_way(wagonik)
    print(f'Najmniejszy czas: {b.find_best_possible_ride(file_name)[-1]}')
    time.sleep(10)
    q = Q_table(read_turns(file_name))
    dict_turns = read_turns(file_name)
    
    quality = 0.325 #parametr quality
    learning = Learning(wagonik, dict_turns, q,b, quality,file_name)
    
    #wywołanie funkcji parametry epsilon, alpha-learning rate, discount rate
    print(learning.learn_trolley_fast_way(0.9, 0.9, 0))
   
    #print(q.q_values)
    #print(read_turns("turns.csv"))
    print("Done")






if __name__ == "__main__":
    main()

